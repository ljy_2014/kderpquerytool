﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Management;
using System.Net.NetworkInformation;
using System.Windows.Forms;

namespace KDErpQueryTool
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (CheckLicense())
            {
                bool canLogin = false;
                using (SqlConnection conn = new SqlConnection(ConfigReader.ConnectString))
                {
                    try
                    {
                        conn.Open();
                        canLogin = true;
                    }
                    catch (Exception e)
                    {
                        Application.Run(new ConnectStringView());
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
                if (canLogin)
                {
                    Application.Run(new QueryEprMainView());
                }
            }
            else
            {
                return;
            }

        }

        private static bool CheckLicense()
        {
            string mac = GetMacByNetworkInterface();
            string guid = GetCpuID();

            var machincode = LicenseHandel.CreateMachineCode(mac + guid);

            string lincense = LicenseHandel.CreateLicense(machincode);

            var Config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var lic = Config.AppSettings.Settings["License"].Value;
            if (lincense != lic)
            {
                Config.AppSettings.Settings["MachineCode"].Value = LicenseHandel.CreateMachineCode(mac + guid);
                Config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
                MessageBox.Show(string.Format("该软件尚未授权，请联系管理员！\n该系统机器码为:\n{0}\n机器码已经写入配置文件", LicenseHandel.CreateMachineCode(mac + guid)), "系统提示！", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        /// <summary>
        /// 获取本地mac地址
        /// </summary>
        /// <returns></returns>
        public static string GetMacByNetworkInterface()
        {
            try
            {
                NetworkInterface[] interfaces = NetworkInterface.GetAllNetworkInterfaces();
                foreach (NetworkInterface ni in interfaces)
                {
                    return BitConverter.ToString(ni.GetPhysicalAddress().GetAddressBytes());
                }
            }
            catch (Exception)
            {
            }
            return "00-00-00-00-00-00";
        }
        /// <summary>
        /// 获取CPu编号
        /// </summary>
        /// <returns></returns>
        private static string GetCpuID()
        {
            try
            {
                string cpuInfo = "";//cpu序列号 
                ManagementClass mc = new ManagementClass("Win32_Processor");
                ManagementObjectCollection moc = mc.GetInstances();
                foreach (ManagementObject mo in moc)
                {
                    cpuInfo = mo.Properties["ProcessorId"].Value.ToString();
                }
                moc = null;
                mc = null;
                return cpuInfo;
            }
            catch
            {
                return "unknow";
            }
            finally
            {
            }

        }
    }
}
