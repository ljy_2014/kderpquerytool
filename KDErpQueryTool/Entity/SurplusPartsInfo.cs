﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDErpQueryTool.Entity
{
    public class SurplusPartsInfo
    {
        public string FNumber { get; set; }
        public string FName { get; set; }
        public string FModel { get; set; }
        public string StorageName { get; set; }
        public double ForderPrice { get; set; }
        public double Fqty { get; set; }
        public string FCreateUser { get; set; }
        public DateTime FCreateDate { get; set; }
        public double InverotyCheck
        {
            get { return ForderPrice * Fqty; }
        }
    }
}
