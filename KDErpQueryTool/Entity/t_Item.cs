﻿using Dapper;
using SqlAccess;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace KDErpQueryTool.Entity
{
    [Table("t_Item")]
    public class ItemModel
    {
        [Column("FItemID")]
        public int FItemID { get; set; }
        [Column("FItemClassID")]
        public int FItemClassID { get; set; }
        [Column("FExternID")]
        public int FExternID { get; set; }
        [Column("FNumber")]
        public string FNumber { get; set; }
        [Column("FParentID")]
        public int FParentID { get; set; }
        [Column("FLevel")]
        public int FLevel { get; set; }
        [Column("FDetail")]
        public bool FDetail { get; set; }
        [Column("FName")]
        public string FName { get; set; }
        //public bool? FunUsed { get; set; }
        //public string FBrNo { get; set; }
        [Column("FFullNumber")]
        public string FFullNumber { get; set; }
        //public bool FDiff { get; set; }
        [Column("FDeleted")]
        public int FDeleted { get; set; }
        [Column("FShortNumber")]
        public string FShortNumber { get; set; }
        [Column("FFullName")]
        public string FFullName { get; set; }
        //public string UUID { get; set; }
        [Column("FGRCommonID")]
        public int FGRCommonID { get; set; }
        [Column("FSystemType")]
        public int FSystemType { get; set; }
        [Column("FUseSign")]
        public int FUseSign { get; set; }
        //public int FChkUserID { get; set; }
        [Column("FAccessory")]
        public int FAccessory { get; set; }
        //public int FGrControl { get; set; }
        //public int FModifyTime { get; set; }
        //public int FHavePicture { get; set; }
        Lazy<DataContext> dc = new Lazy<DataContext>(() => { return new DataContext(ConfigReader.ConnectString); }, true);
        public IEnumerable<ItemModel> Children
        {
            get {
                var cmd = string.Format("select * from t_item where FParentID={0} and FDeleted=0 and FItemClassID=4 and FDetail=0 order by FNumber", FItemID);
                return dc.Value.QueryModels<ItemModel>(cmd);
            }
        }

        public IEnumerable<ItemModel> GetChildren(string cmd)
        {
            return dc.Value.QueryModels<ItemModel>(cmd);
        }

        
    }
}
