﻿using SqlAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDErpQueryTool.Entity
{

    public class Information
    {
        [Column("FNumber")]
        public string PartCode { get; set; }
        [Column("FName")]
        public string PartName { get; set; }
        [Column("FModel")]
        public string PartSpec { get; set; }
        [Column("FErpClsID")]
        public int PartProperty { get; set; }
        [Column("FQty")]
        public decimal FQty { get; set; }
        [Column("FOrderPrice")]
        public double Price { get; set; }
        public double InventoryCheck { get { return (double)FQty * Price; } }
        [Column("FCreateUser")]
        public string Creater { get; set; }
        [Column("FCreateDate")]
        public DateTime CreateTime { get; set; }
    }
}
