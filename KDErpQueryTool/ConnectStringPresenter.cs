﻿using MvpCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;
using Dapper;

namespace KDErpQueryTool
{
    public class ConnectStringPresenter : Presenter<IConnectStringView<ConnectStringModel>>
    {
        public ConnectStringPresenter(IConnectStringView<ConnectStringModel> view) : base(view)
        {
            View.Model = new ConnectStringModel();
            View.InitHandler += Init;
            View.btnSaveHandler += Save;
            //View.btnRefreshHandler += Refresh;
            View.cmbSelectedChangedHandler += SelctedValueChanged;
            View.serverTextLoseFocusHandler += ServerChanged;
            View.cmbMouseEnterHandler += MouseEnter;
        }

        private void MouseEnter(object sender, EventArgs e)
        {
            if (!(View.Cbox.DataSource is List<string>))
            {
                if (string.IsNullOrEmpty(View.Model.Server) || string.IsNullOrEmpty(View.Model.UserName) || string.IsNullOrEmpty(View.Model.Password))
                {
                    return;
                }

                var conn = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3}",
                   View.Model.Server, "master", View.Model.UserName, View.Model.Password);

                using (SqlConnection c = new SqlConnection(conn))
                {
                    try
                    {
                        var res = c.Query<string>("select name from sysdatabases").ToList();
                        View.Cbox.DataSource = res;
                        //View.Model.DbNames = new System.ComponentModel.BindingList<string>(res);
                    }
                    catch (Exception)
                    {
                        
                    }

                }
            }

        }

        private void ServerChanged(object sender, EventArgs e)
        {
            View.Cbox.DataSource = null;
        }

        private void SelctedValueChanged(object sender, EventArgs e)
        {
            var cm = sender as ComboBox;
            if (cm.SelectedItem !=null)
            {
                View.Model.DbName = cm.SelectedItem.ToString();
            }
            
        }

        private void Refresh(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(View.Model.Server)||string.IsNullOrEmpty(View.Model.UserName)||string.IsNullOrEmpty(View.Model.Password))
            {
                return;
            }

            var conn = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3}",
               View.Model.Server, "master", View.Model.UserName, View.Model.Password);

            using (SqlConnection c=new SqlConnection(conn))
            {
                try
                {
                    var res = c.Query<string>("select name from sysdatabases").ToList();
                    View.Cbox.DataSource = res;
                    //View.Model.DbNames = new System.ComponentModel.BindingList<string>(res);
                }
                catch (Exception)
                {
                    View.Status.Invoke(new Action(delegate
                    {
                        View.Status.Text = "数据库连接错误，请检查！";
                    }));
                }
          
            }
        }

        private void Save(object sender, EventArgs e)
        {
            var conn = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3}", 
                View.Model.Server, View.Model.DbName, View.Model.UserName, View.Model.Password);

            using (SqlConnection connection=new SqlConnection(conn))
            {
                try
                {
                    connection.Open();
                }
                catch (Exception)
                {
                    View.Status.Invoke(new Action(delegate
                    {
                        View.Status.Text = "数据库连接错误，请检查！";
                    }));
                    return;
                }

            }
            ConfigReader.ConnectString = conn;

            View.Status.Invoke(new Action(delegate
            {
                View.Status.Text = "数据库连接成功，请重启应用程序！";
                View.MainForm.DialogResult = DialogResult.OK;
            }));

        }

        private void Init(object sender, EventArgs e)
        {
            var conn = ConfigReader.ConnectString;          
        }
    }
}
