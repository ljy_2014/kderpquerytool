﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KDErpQueryTool.Control
{
    public class LazyTreeView : TreeView
    {
        private object _dataSource;
        /// <summary>
        /// 获取或设置数据源
        /// </summary>
        public object DataSource
        {
            get {
                return _dataSource;
            }
            set {
                _dataSource = value;
                OnDataSourceChanged(_dataSource);
            }
        }
        /// <summary>
        /// 当 DataSource 发生改变时触发。
        /// </summary>
        /// <param name="dataSource">数据源</param>
        protected virtual void OnDataSourceChanged(object dataSource)
        {
            RefreshDataSource();
        }
        /// <summary>
        /// 刷新数据源绑定
        /// </summary>
        public virtual void RefreshDataSource()
        {
            Nodes.Clear();

            if (_dataSource != null)
            {
                foreach (var item in GetRootNodes(_dataSource))
                {
                    AddNodes(Nodes, item);
                }
            }
        }

        /// <summary>
        /// 获取 TreeView 的根节点数据。
        /// </summary>
        public Func<object, IEnumerable> GetRootNodes;
        /// <summary>
        /// 获取某一节点的子节点数据。
        /// </summary>
        public Func<object, IEnumerable> GetChildNodes;
        /// <summary>
        /// 获取某一数据对应显示的文本。
        /// </summary>
        public Func<object, string> GetNodeText;
        public Func<object, int> GetNodeImageIndex;
        public Func<object, string> GetNodeImageKey;

        protected virtual void AddNodes(TreeNodeCollection nodes, object item)
        {
            var node = nodes.Add(GetNodeText(item));
            node.Tag = item;
            if (GetNodeImageIndex != null)
            {
                node.ImageIndex = GetNodeImageIndex(item);
                node.SelectedImageIndex = node.ImageIndex;
            }
            if (GetNodeImageKey != null)
            {
                node.ImageKey = GetNodeImageKey(item);
                node.SelectedImageKey = node.ImageKey;
            }

            int levels = 1;
            var node2 = node;
            while (node2.Parent != null)
            {
                levels++;
                node2 = node2.Parent;
            }
            foreach (var child in GetChildNodes(item))
            {
                if (levels == 1)
                {
                    AddNodes(node.Nodes, child);
                }
                else
                {
                    var node3 = node.Nodes.Add("[children]");
                    node3.Tag = "[children]";
                    break;
                }
            }
        }

        protected virtual void AddChildren(TreeNodeCollection nodes, object parent)
        {
            nodes.Clear();
            foreach (var child in GetChildNodes(parent))
            {
                AddNodes(nodes, child);
            }
        }

        protected override void OnAfterExpand(TreeViewEventArgs e)
        {
            if (e.Node.Nodes.Count == 1)
            {
                var first = e.Node.Nodes[0];
                if (string.Format("{0}", first.Tag) == "[children]")
                {
                    AddChildren(e.Node.Nodes, e.Node.Tag);
                }
            }
            base.OnAfterExpand(e);
        }

    }
}
