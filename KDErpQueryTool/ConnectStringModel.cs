﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace KDErpQueryTool
{
    public class ConnectStringModel
    {
        public string Server { get; set; }
        public string DbName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public BindingList<string> DbNames { get; set; }
    }
}
