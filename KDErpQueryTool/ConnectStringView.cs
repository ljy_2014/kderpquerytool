﻿using MvpCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KDErpQueryTool
{
    [PresenterBinding(typeof(ConnectStringPresenter))]
    public partial class ConnectStringView : MvpBaseForm, IConnectStringView<ConnectStringModel>
    {
        public ConnectStringView()
        {
            InitializeComponent();

            this.Load += InitHandler;
            this.btnSave.Click += btnSaveHandler;
            this.btnCancel.Click += btnCancelHandler;
            //this.btnRefresh.Click += btnRefreshHandler;
            this.cmbox.SelectedValueChanged += cmbSelectedChangedHandler;
            this.txtServer.Leave += serverTextLoseFocusHandler;
            this.cmbox.Click += cmbMouseEnterHandler;

            this.Load += LoadBinding;

        }

        private void LoadBinding(object sender, EventArgs e)
        {
            this.txtServer.DataBindings.Add(new Binding("Text", Model, "Server"));
            this.txtUser.DataBindings.Add(new Binding("Text", Model, "UserName"));
            this.txtPassword.DataBindings.Add(new Binding("Text", Model, "Password"));
        }

        public ConnectStringModel Model { get ; set ; }
        public Form MainForm { get { return this; }}

        public ComboBox Cbox { get { return this.cmbox; } }

        public Label Status { get { return this.lblstatus; } }

        public event EventHandler btnSaveHandler;
        public event EventHandler btnCancelHandler;
        public event EventHandler InitHandler;
        //public event EventHandler btnRefreshHandler;
        public event EventHandler cmbSelectedChangedHandler;
        public event EventHandler serverTextLoseFocusHandler;
        public event EventHandler cmbMouseEnterHandler;

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

   
    }
}
