﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace KDErpQueryTool
{
    public static class ConfigReader
    {
        private static Dictionary<string, string> dic = new Dictionary<string, string>();
        //public static string ConnectString2 {
        //    get {
        //        Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        //        var Address = config.AppSettings.Settings["Adress"].Value;
        //        var Satellite = config.AppSettings.Settings["MutilSatellite"].Value;
        //        var Password = config.AppSettings.Settings["Password"].Value;
        //        var Username = config.AppSettings.Settings["Username"].Value;
        //        var IsTemp = config.AppSettings.Settings["Tmp"].Value.ToLower().Equals("true");

        //        //string dbName = "";
        //        //if (IsTemp) {
        //        //    dbName = string.Format("{0}.{1}_TMP", Satellite, Stage);
        //        //}
        //        //else {
        //        //    dbName = string.Format("{0}.{1}", Satellite, Stage);
        //        //}
        //        //return string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};", Address, dbName, Username, Password);
        //    }
        //}
        public static string ConnectTempString
        {
            get {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                return config.AppSettings.Settings["ConnectionString"].Value;
            }
            set {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings["ConnectionString"].Value = value;
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
            }
        }
        public static string GetOrAdd(string key, Func<string> func)
        {
            if (dic.ContainsKey(key))
            {
                return dic[key];
            }
            else
            {
                dic.Add(key, func());
                return func();
            }
        }

        public static string StageConnectString
        {
            get {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                return config.AppSettings.Settings["StageConnectionString"].Value;
            }
            set {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings["StageConnectionString"].Value = value;
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
            }
        }

        public static string HostIP
        {
            get {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                return config.AppSettings.Settings["Adress"].Value;
            }
        }

        public static string ConnectString
        {
            get {
                var Config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                //var connectionString1 = "Data Source=.;Initial Catalog=AIS20170414123624;User ID=sa;Password=handsome1211";
                //return Config.ConnectionStrings.ConnectionStrings["KD-ERP"].ConnectionString;

                //var connectiongString2 = "Password=123456;Persist Security Info=True;User ID=sa;Initial Catalog=zicaierp;Data Source=EK86X6CPAFPQUCG";
                //return connectionString1;
                //return connectiongString2;

                 return Config.AppSettings.Settings["KD-ERP"].Value; 
            }
            set {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings["KD-ERP"].Value = value;
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
            }
        }

    }
}

