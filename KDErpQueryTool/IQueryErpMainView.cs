﻿using KDErpQueryTool.Control;
using MvpCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KDErpQueryTool
{
    public interface IQueryErpMainView<T> : IView<T>
    {
        Form Main { get; }
        DataGridView GridView { get; }
        LazyTreeView CategoryTree { get; }
        DateTime StartTime { get; }
        DateTime EndTime { get; }
        Label AllCount { get; }
        Label ThisCount { get; }
        TextBox DayLimit { get; }
        ToolStripStatusLabel SearchingText { get; }
        ToolStripProgressBar SearchingBar { get; }

        event EventHandler InitialCatogoryEventHandler;

        event EventHandler StartSearchEventHandler;

        event EventHandler ExportEventHandler;

        event EventHandler SearchSurplusPartsEventHandler;

        event EventHandler SearchDuplicatePartsEventHandler;

        event EventHandler SearchPartEventHandler;

        event TreeNodeMouseClickEventHandler SelectedCategoryEventHandler;

        event DataGridViewCellFormattingEventHandler DataGridCellFormattingEventHandler;

    }
}
