﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dapper;
using KDErpQueryTool.Entity;
using MvpCore;
using SqlAccess;

namespace KDErpQueryTool
{
    public class QueryErpMainPresenter : Presenter<IQueryErpMainView<QueryErpMainModel>>
    {
        public QueryErpMainPresenter(IQueryErpMainView<QueryErpMainModel> view)
            : base(view)
        {
            View.Model = new QueryErpMainModel();
            View.InitialCatogoryEventHandler += InitCategory;
            //View.InitialCatogoryEventHandler += GenerateTree;
            View.StartSearchEventHandler += StartSearch;
            View.SelectedCategoryEventHandler += SelectedCategory;
            View.ExportEventHandler += ExportParts;
            View.DataGridCellFormattingEventHandler += PartDetailDataGridCellFormatting;
            View.SearchSurplusPartsEventHandler += SearchSurplus;
            View.SearchDuplicatePartsEventHandler += SearchDuplicate;
            View.SearchPartEventHandler += SearchPart;
        }

        private void SearchPart(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(View.DayLimit.Text))
            {
                MessageBox.Show("请输入搜索内容", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            StringBuilder cmd = new StringBuilder();
            cmd.Append("SELECT t2.FItemID, \n");
            cmd.Append("       t1.FNumber         AS PartCode, \n");
            cmd.Append("       t1.FName           AS PartName, \n");
            cmd.Append("       t3.FModel          AS PartSpec, \n");
            cmd.Append("       t4.FErpClsID       AS PartProperty, \n");
            cmd.Append("       t3.FOrderPrice     AS Price, \n");
            cmd.Append("       Isnull(t5.FQty, 0) AS FQty, \n");
            cmd.Append("       t2.FCreateUser     AS Creater, \n");
            cmd.Append("       t2.FCreateDate     AS CreateTime \n");
            cmd.Append("FROM   t_Item t1 \n");
            cmd.Append("       LEFT JOIN t_BaseProperty t2 \n");
            cmd.Append("              ON t1.FItemID = t2.FItemID \n");
            cmd.Append("       LEFT JOIN t_ICItemCore t3 \n");
            cmd.Append("              ON t1.FItemID = t3.FItemID \n");
            cmd.Append("       LEFT JOIN t_ICItemBase t4 \n");
            cmd.Append("              ON t1.FItemID = t4.FItemID \n");
            cmd.Append("       LEFT JOIN ICInventory t5 \n");
            cmd.Append("              ON t1.FItemID = t5.FItemID \n");
            cmd.Append("WHERE  t1.FItemClassID = 4 \n");
            cmd.Append("       AND FDetail = 1 \n");
            cmd.Append("       AND t1.FDeleted = 0 \n");
            cmd.Append("       AND t2.FTypeID=3 \n");

            cmd.AppendFormat("       AND ( t1.FName LIKE '%{0}%' \n", View.DayLimit.Text.Trim());
            cmd.AppendFormat("              OR t1.FNumber LIKE '%{0}%' )", View.DayLimit.Text.Trim());

            using (SqlConnection conn = new SqlConnection(ConfigReader.ConnectString))
            {
                //var res = dc.QueryModels<Information>(cmd.ToString()).ToList();
                var res = conn.Query<Information>(cmd.ToString());
                View.GridView.Columns.Clear();
                View.GridView.Columns.AddRange(PartInfoColumns());
                View.GridView.DataSource = res;

                View.AllCount.Text = "查询物料数量：" + res.Count();
                View.ThisCount.Text = "查询物料核算：" + Math.Round(res.Sum(x => x.InventoryCheck), 2);
            }


        }

        private void SearchDuplicate(object sender, EventArgs e)
        {
            StringBuilder cmd = new StringBuilder();
            cmd.Append("SELECT t2.FItemID, \n");
            cmd.Append("       t1.FNumber, \n");
            cmd.Append("       t1.FName, \n");
            cmd.Append("       t3.FModel, \n");
            cmd.Append("       t4.FErpClsID, \n");
            cmd.Append("       t3.FOrderPrice, \n");
            cmd.Append("       Isnull(t5.FQty, 0) AS FQty, \n");
            cmd.Append("       t2.FCreateUser, \n");
            cmd.Append("       t2.FCreateDate \n");
            cmd.Append("FROM   t_Item t1 \n");
            cmd.Append("       LEFT JOIN t_BaseProperty t2 \n");
            cmd.Append("              ON t1.FItemID = t2.FItemID \n");
            cmd.Append("       LEFT JOIN t_ICItemCore t3 \n");
            cmd.Append("              ON t1.FItemID = t3.FItemID \n");
            cmd.Append("       LEFT JOIN t_ICItemBase t4 \n");
            cmd.Append("              ON t1.FItemID = t4.FItemID \n");
            cmd.Append("       LEFT JOIN ICInventory t5 \n");
            cmd.Append("              ON t1.FItemID = t5.FItemID \n");
            cmd.Append("WHERE  t1.FItemClassID = 4 \n");
            cmd.Append("       AND FDetail = 1 \n");
            cmd.Append("       AND t1.FDeleted = 0 \n");
            cmd.Append("       AND t2.FTypeID=3 \n");
            cmd.Append("       AND t1.FNumber IN(SELECT FNumber \n");
            cmd.Append("                         FROM   t_item \n");
            cmd.Append("                         WHERE  FDetail = 1 \n");
            cmd.Append("                                AND FItemClassID = 4 \n");
            cmd.Append("                         GROUP  BY FNumber \n");
            cmd.Append("                         HAVING Count(Fnumber) > 1) \n");
            cmd.Append("ORDER  BY t1. FNumber");

            using (SqlConnection conn = new SqlConnection(ConfigReader.ConnectString))
            {
                var res = conn.Query<Information>(cmd.ToString());
                View.GridView.Columns.Clear();
                View.GridView.Columns.AddRange(PartInfoColumns());
                View.GridView.DataSource = res;
            }

        }

        private void SearchSurplus(object sender, EventArgs e)
        {
            if (double.TryParse(View.DayLimit.Text, out double limit))
            {
                if (limit == 0)
                {
                    MessageBox.Show("时间设置不能为0", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            else
            {
                MessageBox.Show("请输入时间间隔", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            try
            {
                View.SearchingText.Visible = true;
                View.SearchingBar.Visible = true;
                using (SqlConnection conn = new SqlConnection(ConfigReader.ConnectString))
                {
                   
                    StringBuilder varname1 = new StringBuilder();
                    varname1.Append("SELECT t8.FItemID, \n");
                    varname1.Append("       t8.FNumber, \n");
                    varname1.Append("       t8.FName, \n");
                    varname1.Append("       t8.FModel, \n");
                    varname1.Append("       t8.StorageName, \n");
                    varname1.Append("       t8.FQty, \n");
                    varname1.Append("       t8.FOrderPrice, \n");
                    varname1.Append("       t9.FCreateDate, \n");
                    varname1.Append("       t9.FCreateUser \n");
                    varname1.Append("FROM   (SELECT t1.FItemID, \n");
                    varname1.Append("               t1.fnumber, \n");
                    varname1.Append("               t1.FName, \n");
                    varname1.Append("               t1.FModel, \n");
                    varname1.Append("               t2.FName AS StorageName, \n");
                    varname1.Append("               w1.fqty, \n");
                    varname1.Append("               t5.FOrderPrice \n");
                    varname1.Append("        FROM   ICInventory w1, \n");
                    varname1.Append("               t_ICItem t1, \n");
                    varname1.Append("               t_Stock t2, \n");
                    varname1.Append("               t_MeasureUnit t3, \n");
                    varname1.Append("               t_MeasureUnit t4, \n");
                    varname1.Append("               t_ICItemCore t5 \n");
                    varname1.Append("        WHERE  w1.FItemID = t1.FItemID \n");
                    varname1.Append("               AND w1.FStockID = t2.FItemID \n");
                    varname1.Append("               AND w1.FQty <> 0 \n");
                    varname1.Append("               AND t1.FUnitID = t3.FMeasureUnitID \n");
                    varname1.Append("               AND t1.FStoreUnitID = t4.FMeasureUnitID \n");
                    varname1.Append("               AND w1.FItemID IN (SELECT S.FItemID \n");
                    varname1.Append("                                  FROM   (SELECT v1.FItemID, \n");
                    varname1.Append("                                                 u1.FDate \n");
                    varname1.Append("                                          FROM   ICStockBill u1 \n");
                    varname1.Append("                                                 INNER JOIN ICStockBillEntry v1 \n");
                    varname1.Append("                                                         ON u1.FInterID = v1.FInterID \n");
                    varname1.Append("                                                            AND u1.FCancellation = 0 \n");
                    varname1.Append("                                          UNION ALL \n");
                    varname1.Append("                                          SELECT u1.FItemID, \n");
                    varname1.Append("                                                 u1.FStockInDate AS FDate \n");
                    varname1.Append("                                          FROM   ICInvInitial u1 \n");
                    varname1.Append("                                          WHERE  u1.Fbegqty <> 0) S \n");
                    varname1.Append("                                  GROUP  BY S.FItemID \n");
                    varname1.Append("                                  HAVING Datediff(DD, Max(FDate), Getdate()) > {0}) \n");
                    varname1.Append("               AND w1.FItemID = t5.FItemID \n");
                    varname1.Append("        GROUP  BY t1.FItemID, \n");
                    varname1.Append("                  t1.FNumber, \n");
                    varname1.Append("                  t1.FShortNumber, \n");
                    varname1.Append("                  t1.FName, \n");
                    varname1.Append("                  t1.FModel, \n");
                    varname1.Append("                  w1.FBatchNo, \n");
                    varname1.Append("                  w1.FStockID, \n");
                    varname1.Append("                  t2.FNumber, \n");
                    varname1.Append("                  t2.FShortNumber, \n");
                    varname1.Append("                  t2.FName, \n");
                    varname1.Append("                  t3.FName, \n");
                    varname1.Append("                  t4.FName, \n");
                    varname1.Append("                  w1.fqty, \n");
                    varname1.Append("                  t5.FOrderPrice) t8 \n");
                    varname1.Append("       LEFT JOIN t_BaseProperty t9 \n");
                    varname1.Append("              ON t8.FItemID = t9.FItemID");
                    varname1.Append("       Where t9.FTypeID=3");


                    var res = conn.Query<SurplusPartsInfo>(string.Format(varname1.ToString(), limit), commandTimeout: 3600);

                    View.GridView.Columns.Clear();
                    View.GridView.Columns.AddRange(SurplusPartColumns());
                    View.GridView.DataSource = res;

                    View.AllCount.Text = "呆滞物料数量：" + res.Count();
                    View.ThisCount.Text = "呆滞物料核算：" + Math.Round(res.Sum(x => x.InverotyCheck), 2);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                View.SearchingText.Visible = false;
                View.SearchingBar.Visible = false;
            }



        }
        private DataGridViewColumn[] SurplusPartColumns()
        {
            DataGridViewColumn[] cols1 = new DataGridViewColumn[]
            {
                new DataGridViewTextBoxColumn
                {
                    HeaderText="物料编码",
                    DataPropertyName="FNumber",
                    Name="FNumber"
                },
                new DataGridViewTextBoxColumn
                {
                    HeaderText="物料名称",
                    DataPropertyName="FName",
                    Name="FName"
                },
                new DataGridViewTextBoxColumn
                {
                    HeaderText="物料规格",
                    DataPropertyName="FModel",
                    Name="FModel"
                },
                new DataGridViewTextBoxColumn
                {
                    HeaderText="物料仓库",
                    DataPropertyName="StorageName",
                    Name="StorageName"
                },
                new DataGridViewTextBoxColumn
                {
                    HeaderText="物料库存",
                    DataPropertyName="Fqty",
                    Name="Fqty"
                },
                new DataGridViewTextBoxColumn
                {
                    HeaderText="采购单价",
                    DataPropertyName="ForderPrice",
                    Name="ForderPrice"
                },
                new DataGridViewTextBoxColumn
                {
                    HeaderText="存货核算",
                    DataPropertyName="InverotyCheck",
                    Name="InverotyCheck"
                },
                new DataGridViewTextBoxColumn
                {
                    HeaderText="创建人",
                    DataPropertyName="FCreateUser",
                    Name="Creater"
                },
                new DataGridViewTextBoxColumn
                {
                    HeaderText="创建时间",
                    DataPropertyName="FCreateDate",
                    Name="CreateTime"
                },
           };

            return cols1;
        }
        private DataGridViewColumn[] PartInfoColumns()
        {
            DataGridViewColumn[] cols = new DataGridViewColumn[]
            {
                new DataGridViewTextBoxColumn
                {
                    HeaderText="物料编码",
                    DataPropertyName="PartCode",
                    Name="PartCode"
                },
                new DataGridViewTextBoxColumn
                {
                    HeaderText="物料名称",
                    DataPropertyName="PartName",
                    Name="PartName"
                },
                new DataGridViewTextBoxColumn
                {
                    HeaderText="物料规格",
                    DataPropertyName="PartSpec",
                    Name="PartSpec"
                },
                new DataGridViewTextBoxColumn
                {
                    HeaderText="物料属性",
                    DataPropertyName="PartProperty",
                    Name="PartProperty"
                },
                new DataGridViewTextBoxColumn
                {
                    HeaderText="物料库存",
                    DataPropertyName="Fqty",
                    Name="Fqty"
                },
                new DataGridViewTextBoxColumn
                {
                    HeaderText="采购单价",
                    DataPropertyName="Price",
                    Name="Price"
                },
                new DataGridViewTextBoxColumn
                {
                    HeaderText="存货核算",
                    DataPropertyName="InventoryCheck",
                    Name="InventoryCheck"
                },
                new DataGridViewTextBoxColumn
                {
                    HeaderText="创建人",
                    DataPropertyName="Creater",
                    Name="Creater"
                },
                new DataGridViewTextBoxColumn
                {
                    HeaderText="创建时间",
                    DataPropertyName="CreateTime",
                    Name="CreateTime"
                },
            };
            return cols;
        }
        private void PartDetailDataGridCellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            var dwg = sender as DataGridView;
            var entity = dwg.Rows[e.RowIndex].DataBoundItem as Information;
            if (entity == null)
            {
                return;
            }
            var col = dwg.Columns[e.ColumnIndex];
            switch (col.DataPropertyName)
            {
                case "PartProperty":
                    switch (entity.PartProperty)
                    {
                        case 1:
                            e.Value = "外购";
                            break;
                        case 2:
                            e.Value = "自制";
                            break;
                        case 3:
                            e.Value = "委外加工";
                            break;
                        case 5:
                            e.Value = "虚拟";
                            break;
                        case 6:
                            e.Value = "特征类";
                            break;
                        case 7:
                            e.Value = "配置类";
                            break;
                        case 8:
                            e.Value = "规划类";
                            break;
                        case 9:
                            e.Value = "组装件";
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        DataContext dc = new DataContext(ConfigReader.ConnectString);
        private void InitCategory(object sender, EventArgs e)
        {

            if (dc.TestConnection())
            {
                View.Main.Invoke(new Action(() =>
                {
                    View.SearchingText.Visible = true;
                    View.SearchingBar.Visible = true;
                }));

                var end = View.EndTime.Date.AddDays(1);
                var start = View.StartTime.Date;
                var top = dc.QueryModels<ItemModel>("select * from t_item where Flevel=1 and FItemClassID=4 and FDetail=0 order by FNumber");

                View.CategoryTree.Nodes.Clear();
                View.CategoryTree.Nodes.AddRange(ChangeToNodes(top, start, end).ToArray());
                View.CategoryTree.ExpandAll();

                var cmd = "select count(*) from t_item t1 left join t_BaseProperty t2 on t1.FItemID=t2.FItemID where FItemClassID=4 and FDetail=1  and FDeleted=0 and t2.FTypeID=3 ";
                //var range = string.Format(" and FCreateDate between '{0}' and '{1}'", start.ToString("yyyy-MM-dd HH:mm:ss"), end.ToString("yyyy-MM-dd HH:mm:ss"));
                var res = dc.GetCount(cmd /*+ range*/);
                View.AllCount.Invoke(new Action(() =>
                {
                    View.AllCount.Text = "全部物料总数量：" + res;
                }));

                var parttern = "select t2.FItemID,t1.FNumber ,t1.FName,t3.FModel,t4.FErpClsID,t3.FOrderPrice ,ISNULL(t5.FQty,0) as FQty,t2.FCreateUser ,t2.FCreateDate "
                   + "from t_Item t1 "
                   + "left join t_BaseProperty t2 on t1.FItemID=t2.FItemID "
                   + "left join t_ICItemCore t3 on t1.FItemID=t3.FItemID "
                   + "left join t_ICItemBase t4 on t1.FItemID=t4.FItemID "
                   + "left join ICInventory t5 on t1.FItemID=t5.FItemID "
                   + "where t1.FItemClassID=4  and FDetail=1 and t1.FDeleted=0 and t2.FTypeID=3 ";

                //var range1 = string.Format(" and t2.FCreateDate between '{0}' and '{1}'", View.StartTime.Date.ToString("yyyy-MM-dd HH:mm:ss"), View.EndTime.Date.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss"));

                var result = dc.QueryModels<Information>(parttern /*+ range1*/);
                double total = 0;
                foreach (var item in result)
                {
                    if (item.FQty > 0)
                    {
                        total += (double)item.FQty * item.Price;
                    }
                }

                View.Main.Invoke(new Action(() =>
                {
                    View.ThisCount.Text = "全部物料核算：" + Math.Round(total, 2);
                    View.SearchingText.Visible = false;
                    View.SearchingBar.Visible = false;
                }));

            }
        }

        private void GenerateTree(object sender, EventArgs e)
        {
            View.CategoryTree.GetRootNodes = x => x as IEnumerable;
            View.CategoryTree.GetChildNodes = x =>
            {
                if (x is ItemModel)
                {
                    return GetChildrenPart(x as ItemModel);
                }
                return null;
            };
            View.CategoryTree.GetNodeText = x =>
            {
                if (x is ItemModel)
                {
                    var model = x as ItemModel;
                    if (GetChildrenPart(model).Count() == 0)
                    {
                        using (SqlConnection conn = new SqlConnection(ConfigReader.ConnectString))
                        {
                            var countcmd = string.Format("select count(*) from t_item t1 left join t_BaseProperty t2 on t1.FItemID=t2.FItemID where FItemClassID=4 and FDetail=1 and FParentID={0} and FDeleted=0 ", model.FItemID);
                            var res = conn.ExecuteScalar(countcmd);
                            return model.FName + "(" + res + ")";
                        }
                    }
                    return model.FName;
                }
                return null;
            };
            using (SqlConnection conn = new SqlConnection(ConfigReader.ConnectString))
            {
                var cmd = "select * from t_item where Flevel=1 and FItemClassID=4 and FDetail=0 order by FNumber";
                var top = conn.Query<ItemModel>(cmd);

                View.CategoryTree.DataSource = top;
            }
        }
        public IEnumerable<ItemModel> GetChildrenPart(ItemModel father)
        {
            using (SqlConnection conn = new SqlConnection(ConfigReader.ConnectString))
            {
                var cmd = string.Format("select * from t_item where FParentID={0} and FDeleted=0 and FItemClassID=4 and FDetail=0 order by FNumber", father.FItemID);
                return conn.Query<ItemModel>(cmd);
            }
        }

        private IEnumerable<TreeNode> ChangeToNodes(IEnumerable<ItemModel> first, DateTime startTime, DateTime endTime)
        {
            foreach (var item in first)
            {
                var node = new TreeNode
                {
                    Text = item.FName,
                    Tag = item
                };
                if (item.Children.Count() == 0)
                {
                    var cmd = "select count(*) from t_item t1 left join t_BaseProperty t2 on t1.FItemID=t2.FItemID where FItemClassID=4 and FDetail=1 and FParentID={0} and FDeleted=0 and t2.FTypeID=3 ";
                    var range = string.Format(" and FCreateDate between '{0}' and '{1}'", startTime.ToString("yyyy-MM-dd HH:mm:ss"), endTime.ToString("yyyy-MM-dd HH:mm:ss"));
                    var res = dc.GetCount(string.Format(cmd, item.FItemID) + range);
                    if (res == 0)
                    {
                        continue;
                    }
                    node.Text = node.Text + "+（" + dc.GetCount(string.Format(cmd, item.FItemID) + range) + "）";
                }
                else
                {
                    //node.Nodes.AddRange(ChangeToNodes(item.Children).ToArray());
                    var cmd = "select * from t_item where FParentID={0} and FDeleted=0 and FItemClassID=4 and FDetail=0 order by FNumber";

                    var children = ChangeToNodes(item.GetChildren(string.Format(cmd, item.FItemID)), startTime, endTime).ToArray();
                    if (children.Length > 0)
                    {
                        node.Nodes.AddRange(children);
                    }
                    else
                    {
                        continue;
                    }
                }
                yield return node;
            }
        }

        private IEnumerable<TreeNode> ChangeToNodes(IEnumerable<ItemModel> first)
        {
            foreach (var item in first)
            {
                var node = new TreeNode
                {
                    Text = item.FName,
                    Tag = item
                };
                if (item.Children.Count() == 0)
                {
                    var cmd = "select count(*) from t_item t1 left join t_BaseProperty t2 on t1.FItemID=t2.FItemID where FItemClassID=4 and FDetail=1 and FParentID={0} and FDeleted=0 and t2.FTypeID=3 ";
                  
                    var res = dc.GetCount(string.Format(cmd, item.FItemID) );
                    if (res == 0)
                    {
                        continue;
                    }
                    node.Text = node.Text + "+（" + dc.GetCount(string.Format(cmd, item.FItemID) ) + "）";
                }
                else
                {
                    //node.Nodes.AddRange(ChangeToNodes(item.Children).ToArray());
                    var cmd = "select * from t_item where FParentID={0} and FDeleted=0 and FItemClassID=4 and FDetail=0 order by FNumber";

                    var children = ChangeToNodes(item.GetChildren(string.Format(cmd, item.FItemID))).ToArray();
                    if (children.Length > 0)
                    {
                        node.Nodes.AddRange(children);
                    }
                    else
                    {
                        continue;
                    }
                }
                yield return node;
            }
        }


        private void SelectedCategory(object sender, TreeNodeMouseClickEventArgs e)
        {
            var model = e.Node.Tag as ItemModel;
            if (e.Node.Nodes.Count == 0)
            {
                var parttern = "select t2.FItemID,t1.FNumber ,t1.FName,t3.FModel,t4.FErpClsID,t3.FOrderPrice ,ISNULL(t5.FQty,0) as FQty,t2.FCreateUser ,t2.FCreateDate "
                   + "from t_Item t1 "
                   + "left join t_BaseProperty t2 on t1.FItemID=t2.FItemID "
                   + "left join t_ICItemCore t3 on t1.FItemID=t3.FItemID "
                   + "left join t_ICItemBase t4 on t1.FItemID=t4.FItemID "
                   + "left join ICInventory t5 on t1.FItemID=t5.FItemID "
                   + "where t1.FItemClassID=4  and FDetail=1 and t1.FParentID={0} and t1.FDeleted=0 and t2.FTypeID=3";
                var cmd = string.Format(parttern, model.FItemID);
                var range = string.Format(" and t2.FCreateDate between '{0}' and '{1}'", View.StartTime.Date.ToString("yyyy-MM-dd HH:mm:ss"), View.EndTime.Date.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss"));

                var result = dc.QueryModels<Information>(cmd + range).ToList();

                View.GridView.Columns.Clear();
                View.GridView.Columns.AddRange(PartInfoColumns());
                View.GridView.DataSource = result;
            }
        }

        private void StartSearch(object sender, EventArgs e)
        {
            if (dc.TestConnection())
            {
                try
                {
                    View.GridView.Columns.Clear();
                    View.GridView.Columns.AddRange(PartInfoColumns());

                    var start = View.StartTime.Date;
                    var end = View.EndTime.Date.AddDays(1);
                    var cmd = "select * from t_item where Flevel=1 and FItemClassID=4 and FDetail=0 order by FNumber";
                    var range = string.Format(" and FCreateDate between '{0}' and '{1}'", start.ToString("yyyy-MM-dd HH:mm:ss"), end.ToString("yyyy-MM-dd HH:mm:ss"));
                    var top = dc.QueryModels<ItemModel>(cmd);
                    View.CategoryTree.Nodes.Clear();
                    //View.GridView.DataSource = null;
                    View.CategoryTree.Nodes.AddRange(ChangeToNodes(top, start, end).ToArray());
                    View.CategoryTree.ExpandAll();

                    var cmd2 = "select count(*) from t_item t1 left join t_BaseProperty t2 on t1.FItemID=t2.FItemID where FItemClassID=4 and FDetail=1  and FDeleted=0 and t2.FTypeID=3 ";
                    var range2 = string.Format(" and FCreateDate between '{0}' and '{1}'", start.ToString("yyyy-MM-dd HH:mm:ss"), end.ToString("yyyy-MM-dd HH:mm:ss"));
                    var res = dc.GetCount(cmd2 + range2);
                    View.AllCount.Invoke(new Action(() =>
                    {
                        View.AllCount.Text = "新增物料总数量：" + res;
                    }));


                    var parttern = "select t2.FItemID,t1.FNumber ,t1.FName,t3.FModel,t4.FErpClsID,t3.FOrderPrice ,ISNULL(t5.FQty,0) as FQty,t2.FCreateUser ,t2.FCreateDate "
                   + "from t_Item t1 "
                   + "left join t_BaseProperty t2 on t1.FItemID=t2.FItemID "
                   + "left join t_ICItemCore t3 on t1.FItemID=t3.FItemID "
                   + "left join t_ICItemBase t4 on t1.FItemID=t4.FItemID "
                   + "left join ICInventory t5 on t1.FItemID=t5.FItemID "
                   + "where t1.FItemClassID=4  and FDetail=1 and t1.FDeleted=0 and t2.FTypeID=3 ";

                    var range1 = string.Format(" and t2.FCreateDate between '{0}' and '{1}'", View.StartTime.ToString("yyyy-MM-dd HH:mm:ss"), View.EndTime.ToString("yyyy-MM-dd HH:mm:ss"));

                    var result = dc.QueryModels<Information>(parttern + range1);
                    double total = 0;
                    foreach (var item in result)
                    {
                        if (item.FQty > 0)
                        {
                            total += (double)item.FQty * item.Price;
                        }
                    }
                    View.ThisCount.Invoke(new Action(() =>
                    {
                        View.ThisCount.Text = "新增物料核算：" + Math.Round(total, 2);
                    }));
                }
                catch (Exception)
                {
                }


            }
        }

        private void ExportParts(object sender, EventArgs e)
        {
            ExportExcel exportExcel = new ExportExcel();
            exportExcel.GridToExcel("导出物料", View.GridView);
        }


    }
}