﻿using MvpCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KDErpQueryTool
{
    public interface IConnectStringView<T> : IView<T>
    {
        event EventHandler InitHandler;
        event EventHandler btnSaveHandler;
        event EventHandler btnCancelHandler;
        //event EventHandler btnRefreshHandler;
        event EventHandler cmbSelectedChangedHandler;
        event EventHandler serverTextLoseFocusHandler;
        event EventHandler cmbMouseEnterHandler;
        Form MainForm { get; }
        ComboBox Cbox { get; }
        Label Status { get; }
    }
}
