﻿using KDErpQueryTool.Control;
using MvpCore;
using System;
using System.Configuration;
using System.Management;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace KDErpQueryTool
{
    [PresenterBinding(typeof(QueryErpMainPresenter))]
    public partial class QueryEprMainView : MvpBaseForm, IQueryErpMainView<QueryErpMainModel>
    {
        public QueryEprMainView()
        {
            InitializeComponent();
            this.Load += InitialCatogoryEventHandler;
            this.btnSearch.Click += StartSearchEventHandler;
            this.btnExport.Click += ExportEventHandler;
            this.btnSurplus.Click += SearchSurplusPartsEventHandler;
            this.btnDuplication.Click += SearchDuplicatePartsEventHandler; ;
            this.CategoryTree.NodeMouseClick += SelectedCategoryEventHandler;
            this.dwg_partdetail.CellFormatting += DataGridCellFormattingEventHandler;
            this.btnSearchPart.Click += SearchPartEventHandler;

            InitTime();
        }

        


        public Form Main { get { return this; } }
        public LazyTreeView CategoryTree { get { return partCategoryTree; } }
        public QueryErpMainModel Model { get; set; }
        public DateTime StartTime { get { return dtpStart.Value; } }
        public DateTime EndTime { get { return dtpEnd.Value; } }
        public DataGridView GridView { get { return dwg_partdetail; } }
        public Label AllCount { get { return label1; } }
        public Label ThisCount { get { return label2; } }
        public TextBox DayLimit { get { return txtDay; } }
        public ToolStripStatusLabel SearchingText { get { return tsspSeaeching; } }
        public ToolStripProgressBar SearchingBar { get { return tsspSeaechingprocess; } }

        public event EventHandler InitialCatogoryEventHandler;
        public event EventHandler StartSearchEventHandler;
        public event EventHandler ExportEventHandler;
        public event EventHandler SearchSurplusPartsEventHandler;
        public event EventHandler SearchDuplicatePartsEventHandler;
        public event EventHandler SearchPartEventHandler;
        public event TreeNodeMouseClickEventHandler SelectedCategoryEventHandler;
        public event DataGridViewCellFormattingEventHandler DataGridCellFormattingEventHandler;

        private void InitTime()
        {
            //var timer = new System.Windows.Forms.Timer
            //{
            //    Interval = 100
            //};
            //timer.Tick += UpdateTime;
            //timer.Start();
            dtpStart.Value = DateTime.Now.AddYears(-5);
            dtpEnd.Value = DateTime.Now;
        }

        private void UpdateTime(object sender, EventArgs e)
        {
            this.TimeStatus.Text = DateTime.Now.ToString("F");
        }
    }

    public static class LicenseHandel
    {
        public static string CreateLicense(string mac)
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 1; i < 60; i++)
            {
                mac = MD5Encrypt32(mac);
                if (i > 50)
                {
                    stringBuilder.Append(mac);
                }
            }
            return stringBuilder.ToString();
        }

        private static string MD5Encrypt32(string value)
        {
            string text = "";
            MD5 mD = MD5.Create();
            byte[] array = mD.ComputeHash(Encoding.UTF8.GetBytes(value));
            for (int i = 0; i < array.Length; i++)
            {
                text += array[i].ToString("X");
            }
            return text;
        }

        public static string CreateMachineCode(string mac)
        {
            return MD5Encrypt32(mac);
        }
    }
}
