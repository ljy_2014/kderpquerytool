﻿namespace KDErpQueryTool
{
    partial class QueryEprMainView
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dwg_partdetail = new System.Windows.Forms.DataGridView();
            this.PartCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PartName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PartSpec = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PartProperty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PartInventory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InventoryCheck = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Creater = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CreateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSearchPart = new System.Windows.Forms.Button();
            this.txtDay = new System.Windows.Forms.TextBox();
            this.btnDuplication = new System.Windows.Forms.Button();
            this.btnSurplus = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnExport = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.dtpEnd = new System.Windows.Forms.DateTimePicker();
            this.dtpStart = new System.Windows.Forms.DateTimePicker();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsspSeaeching = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsspSeaechingprocess = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.TimeStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.partCategoryTree = new KDErpQueryTool.Control.LazyTreeView();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dwg_partdetail)).BeginInit();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.72498F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 78.27502F));
            this.tableLayoutPanel1.Controls.Add(this.dwg_partdetail, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.statusStrip1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.partCategoryTree, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1384, 570);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // dwg_partdetail
            // 
            this.dwg_partdetail.AllowUserToAddRows = false;
            this.dwg_partdetail.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dwg_partdetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dwg_partdetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PartCode,
            this.PartName,
            this.PartSpec,
            this.PartProperty,
            this.PartInventory,
            this.Price,
            this.InventoryCheck,
            this.Creater,
            this.CreateTime});
            this.dwg_partdetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dwg_partdetail.Location = new System.Drawing.Point(303, 51);
            this.dwg_partdetail.Name = "dwg_partdetail";
            this.dwg_partdetail.RowTemplate.Height = 23;
            this.dwg_partdetail.Size = new System.Drawing.Size(1078, 496);
            this.dwg_partdetail.TabIndex = 1;
            // 
            // PartCode
            // 
            this.PartCode.DataPropertyName = "PartCode";
            this.PartCode.HeaderText = "物料编码";
            this.PartCode.Name = "PartCode";
            // 
            // PartName
            // 
            this.PartName.DataPropertyName = "PartName";
            this.PartName.HeaderText = "物料名称";
            this.PartName.Name = "PartName";
            // 
            // PartSpec
            // 
            this.PartSpec.DataPropertyName = "PartSpec";
            this.PartSpec.HeaderText = "物料规格";
            this.PartSpec.Name = "PartSpec";
            // 
            // PartProperty
            // 
            this.PartProperty.DataPropertyName = "PartProperty";
            this.PartProperty.HeaderText = "物料属性";
            this.PartProperty.Name = "PartProperty";
            // 
            // PartInventory
            // 
            this.PartInventory.DataPropertyName = "PartInventory";
            this.PartInventory.HeaderText = "物料库存";
            this.PartInventory.Name = "PartInventory";
            // 
            // Price
            // 
            this.Price.DataPropertyName = "Price";
            this.Price.HeaderText = "采购单价";
            this.Price.Name = "Price";
            // 
            // InventoryCheck
            // 
            this.InventoryCheck.DataPropertyName = "InventoryCheck";
            this.InventoryCheck.HeaderText = "存货核算";
            this.InventoryCheck.Name = "InventoryCheck";
            // 
            // Creater
            // 
            this.Creater.DataPropertyName = "Creater";
            this.Creater.HeaderText = "创建人";
            this.Creater.Name = "Creater";
            // 
            // CreateTime
            // 
            this.CreateTime.DataPropertyName = "CreateTime";
            this.CreateTime.HeaderText = "创建时间";
            this.CreateTime.Name = "CreateTime";
            // 
            // panel1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel1, 2);
            this.panel1.Controls.Add(this.btnSearchPart);
            this.panel1.Controls.Add(this.txtDay);
            this.panel1.Controls.Add(this.btnDuplication);
            this.panel1.Controls.Add(this.btnSurplus);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnExport);
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Controls.Add(this.dtpEnd);
            this.panel1.Controls.Add(this.dtpStart);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1378, 42);
            this.panel1.TabIndex = 2;
            // 
            // btnSearchPart
            // 
            this.btnSearchPart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearchPart.Location = new System.Drawing.Point(550, 11);
            this.btnSearchPart.Name = "btnSearchPart";
            this.btnSearchPart.Size = new System.Drawing.Size(75, 23);
            this.btnSearchPart.TabIndex = 10;
            this.btnSearchPart.Text = "查询物料";
            this.btnSearchPart.UseVisualStyleBackColor = true;
            // 
            // txtDay
            // 
            this.txtDay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDay.Location = new System.Drawing.Point(456, 12);
            this.txtDay.Name = "txtDay";
            this.txtDay.Size = new System.Drawing.Size(88, 21);
            this.txtDay.TabIndex = 9;
            // 
            // btnDuplication
            // 
            this.btnDuplication.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDuplication.Location = new System.Drawing.Point(713, 11);
            this.btnDuplication.Name = "btnDuplication";
            this.btnDuplication.Size = new System.Drawing.Size(75, 23);
            this.btnDuplication.TabIndex = 8;
            this.btnDuplication.Text = "物料查重";
            this.btnDuplication.UseVisualStyleBackColor = true;
            // 
            // btnSurplus
            // 
            this.btnSurplus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSurplus.Location = new System.Drawing.Point(631, 11);
            this.btnSurplus.Name = "btnSurplus";
            this.btnSurplus.Size = new System.Drawing.Size(75, 23);
            this.btnSurplus.TabIndex = 7;
            this.btnSurplus.Text = "呆滞物料";
            this.btnSurplus.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(1293, 11);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "系统设置";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(174, 18);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "新增物料核算：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 18);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "新增物料总数量：";
            // 
            // btnExport
            // 
            this.btnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExport.Location = new System.Drawing.Point(1175, 11);
            this.btnExport.Margin = new System.Windows.Forms.Padding(2);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(75, 23);
            this.btnExport.TabIndex = 3;
            this.btnExport.Text = "导出物料";
            this.btnExport.UseVisualStyleBackColor = true;
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Location = new System.Drawing.Point(1097, 11);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Text = "新增物料";
            this.btnSearch.UseVisualStyleBackColor = true;
            // 
            // dtpEnd
            // 
            this.dtpEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpEnd.Location = new System.Drawing.Point(952, 11);
            this.dtpEnd.Name = "dtpEnd";
            this.dtpEnd.Size = new System.Drawing.Size(130, 21);
            this.dtpEnd.TabIndex = 1;
            // 
            // dtpStart
            // 
            this.dtpStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpStart.Location = new System.Drawing.Point(811, 11);
            this.dtpStart.Name = "dtpStart";
            this.dtpStart.Size = new System.Drawing.Size(130, 21);
            this.dtpStart.TabIndex = 0;
            this.dtpStart.Value = new System.DateTime(2015, 1, 1, 0, 0, 0, 0);
            // 
            // statusStrip1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.statusStrip1, 2);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsspSeaeching,
            this.tsspSeaechingprocess,
            this.toolStripStatusLabel2,
            this.TimeStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 550);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1384, 20);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsspSeaeching
            // 
            this.tsspSeaeching.Name = "tsspSeaeching";
            this.tsspSeaeching.Size = new System.Drawing.Size(65, 15);
            this.tsspSeaeching.Text = "正在查询...";
            this.tsspSeaeching.Visible = false;
            // 
            // tsspSeaechingprocess
            // 
            this.tsspSeaechingprocess.Name = "tsspSeaechingprocess";
            this.tsspSeaechingprocess.Size = new System.Drawing.Size(100, 14);
            this.tsspSeaechingprocess.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.tsspSeaechingprocess.Visible = false;
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(1193, 15);
            this.toolStripStatusLabel2.Spring = true;
            // 
            // TimeStatus
            // 
            this.TimeStatus.Name = "TimeStatus";
            this.TimeStatus.Size = new System.Drawing.Size(176, 15);
            this.TimeStatus.Text = "本系统所有分析数据仅供参考！";
            // 
            // partCategoryTree
            // 
            this.partCategoryTree.DataSource = null;
            this.partCategoryTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.partCategoryTree.Location = new System.Drawing.Point(3, 51);
            this.partCategoryTree.Name = "partCategoryTree";
            this.partCategoryTree.Size = new System.Drawing.Size(294, 496);
            this.partCategoryTree.TabIndex = 4;
            // 
            // QueryEprMainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1384, 570);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "QueryEprMainView";
            this.Text = "K3 ERP 物料分析工具 V 1.0";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dwg_partdetail)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridView dwg_partdetail;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker dtpEnd;
        private System.Windows.Forms.DateTimePicker dtpStart;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DataGridViewTextBoxColumn PartCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn PartName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PartSpec;
        private System.Windows.Forms.DataGridViewTextBoxColumn PartProperty;
        private System.Windows.Forms.DataGridViewTextBoxColumn PartInventory;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn InventoryCheck;
        private System.Windows.Forms.DataGridViewTextBoxColumn Creater;
        private System.Windows.Forms.DataGridViewTextBoxColumn CreateTime;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tsspSeaeching;
        private System.Windows.Forms.ToolStripProgressBar tsspSeaechingprocess;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;

        private System.Windows.Forms.Button btnDuplication;
        private System.Windows.Forms.Button btnSurplus;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtDay;
        private System.Windows.Forms.ToolStripStatusLabel TimeStatus;
        private Control.LazyTreeView partCategoryTree;
        private System.Windows.Forms.Button btnSearchPart;
    }
}

