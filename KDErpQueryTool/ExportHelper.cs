﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KDErpQueryTool
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Forms;
    using System.Text;
    using NPOI.HSSF.UserModel;
    using System.IO;
    using NPOI.SS.UserModel;

    public class ExportExcel
    {
        public void GridToExcel(string fileName, DataGridView dgv)
        {
            if (dgv.Rows.Count == 0)
            {
                return;
            }
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel 2003格式|*.xls";
            sfd.FileName = fileName + DateTime.Now.ToString("yyyyMMddHHmmssms");
            if (sfd.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            HSSFWorkbook wb = new HSSFWorkbook();
            HSSFSheet sheet = (HSSFSheet)wb.CreateSheet(fileName);
            HSSFRow headRow = (HSSFRow)sheet.CreateRow(0);
            for (int i = 0; i < dgv.Columns.Count; i++)
            {
                HSSFCell headCell = (HSSFCell)headRow.CreateCell(i, CellType.String);
                headCell.SetCellValue(dgv.Columns[i].HeaderText);
            }
            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                HSSFRow row = (HSSFRow)sheet.CreateRow(i + 1);
                for (int j = 0; j < dgv.Columns.Count; j++)
                {
                    HSSFCell cell = (HSSFCell)row.CreateCell(j);
                    if (dgv.Rows[i].Cells[j].FormattedValue  == null)
                    {
                        cell.SetCellType(CellType.Blank);
                    }
                    else
                    {
                        cell.SetCellValue(dgv.Rows[i].Cells[j].FormattedValue.ToString());
                        //if (dgv.Rows[i].Cells[j].ValueType.FullName.Contains("System.Int32"))
                        //{
                        //    cell.SetCellValue(Convert.ToInt32(dgv.Rows[i].Cells[j].Value));
                        //}
                        //else if (dgv.Rows[i].Cells[j].ValueType.FullName.Contains("System.String"))
                        //{
                        //    cell.SetCellValue(dgv.Rows[i].Cells[j].Value.ToString());
                        //}
                        //else if (dgv.Rows[i].Cells[j].ValueType.FullName.Contains("System.Single"))
                        //{
                        //    cell.SetCellValue(Convert.ToSingle(dgv.Rows[i].Cells[j].Value));
                        //}
                        //else if (dgv.Rows[i].Cells[j].ValueType.FullName.Contains("System.Double"))
                        //{
                        //    cell.SetCellValue(Convert.ToDouble(dgv.Rows[i].Cells[j].Value));
                        //}
                        //else if (dgv.Rows[i].Cells[j].ValueType.FullName.Contains("System.Decimal"))
                        //{
                        //    cell.SetCellValue(Convert.ToDouble(dgv.Rows[i].Cells[j].Value));
                        //}
                        //else if (dgv.Rows[i].Cells[j].ValueType.FullName.Contains("System.DateTime"))
                        //{
                        //    cell.SetCellValue(Convert.ToDateTime(dgv.Rows[i].Cells[j].Value).ToString("yyyy-MM-dd"));
                        //}
                    }

                }

            }
            for (int i = 0; i < dgv.Columns.Count; i++)
            {
                sheet.AutoSizeColumn(i);
            }
            using (FileStream fs = new FileStream(sfd.FileName, FileMode.Create))
            {
                wb.Write(fs);
            }
            MessageBox.Show("导出成功！", "导出提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
