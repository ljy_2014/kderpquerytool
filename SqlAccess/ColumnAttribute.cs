﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SqlAccess
{
    [System.AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public sealed class ColumnAttribute : Attribute
    {
        readonly string columnName;
        public ColumnAttribute(string columnName)
        {
            this.columnName = columnName;
        }

        public string ColumnName
        {
            get { return columnName; }
        }

    }
}
