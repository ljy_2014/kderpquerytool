﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlAccess
{

    public class DataContext : IDataContext
    {
        //public DataContext Instance { get { return this; } }
        protected string _connectstring;
        public DataContext(string str)
        {
            _connectstring = str;
        }
        public string ConnectString
        {
            get {
                return _connectstring;
            }
            set {
                _connectstring = value;
            }
        }
        public bool TestConnection()
        {
            using (SqlConnection conn = new SqlConnection(_connectstring))
            {
                try
                {
                    conn.Open();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
                finally
                {
                    conn.Close();
                }
            }
        }
        private string InsertModelSQLString<T>(T model)
        {
            var type = typeof(T);
            var attributes = TypeDescriptor.GetAttributes(model);
            string TableName = string.Empty;
            foreach (Attribute attr in attributes)
            {
                if (attr is TableAttribute)
                {
                    TableName = ((TableAttribute)attr).TableName;
                    break;
                }
            }
            if (TableName == string.Empty)
            {
                TableName = type.Name;
            }
            StringBuilder presb = new StringBuilder("INSERT INTO " + TableName + "(");
            StringBuilder postsb = new StringBuilder("VALUES (");
            var properties = TypeDescriptor.GetProperties(model);
            foreach (PropertyDescriptor prop in properties)
            {
                foreach (Attribute propattr in prop.Attributes)
                {
                    if (propattr is ColumnAttribute)
                    {
                        var columnName = ((ColumnAttribute)propattr).ColumnName;

                        var obj = prop.GetValue(model);

                        if (columnName == string.Empty || obj == null)
                        {
                            continue;
                        }
                        presb.Append(columnName);
                        presb.Append(',');
                        if (prop.PropertyType == typeof(string) || prop.PropertyType == typeof(DateTime))
                        {
                            postsb.Append('\'');
                            postsb.Append(obj);
                            postsb.Append('\'');
                        }
                        else
                        {
                            if (obj is bool)
                            {
                                postsb.Append((bool)obj ? 1 : 0);
                            }
                            else
                            {
                                postsb.Append(obj);
                            }
                        }
                        postsb.Append(',');
                    }
                }
            }
            if (presb[presb.Length - 1] == ',')
            {
                presb.Remove(presb.Length - 1, 1);
                postsb.Remove(postsb.Length - 1, 1);
            }
            presb.Append(")");
            postsb.Append(")");
            presb.Append(postsb);

            return presb.ToString();
        }
        public int InsertModel<T>(T model)
        {
            var insterStr = InsertModelSQLString(model);
            using (SqlConnection conn = new SqlConnection(_connectstring))
            {
                conn.Open();
                try
                {
                    SqlCommand cmd = new SqlCommand(insterStr, conn);
                    var count = cmd.ExecuteNonQuery();
                    return count;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
            }
        }
        public int InsertManyModels<T>(IEnumerable<T> models)
        {
            int i = 0;
            using (SqlConnection conn = new SqlConnection(_connectstring))
            {
                conn.Open();
                foreach (var model in models)
                {
                    var insterStr = InsertModelSQLString(model);
                    var cmd = new SqlCommand(insterStr, conn);
                    i += cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
            return i;
        }
        //public async Task<int> InsertManyModelsAsync<T>(IEnumerable<T> models)
        //{
        //    int i = 0;
        //    using (SqlConnection conn = new SqlConnection(_connectstring))
        //    {
        //        conn.Open();
        //        var tr = conn.BeginTransaction();
        //        try
        //        {
        //            foreach (var model in models)
        //            {
        //                var insterStr = InsertModelSQLString(model);
        //                var cmd = new SqlCommand(insterStr, conn, tr);
        //                i += await cmd.ExecuteNonQueryAsync();
        //            }
        //            tr.Commit();
        //        }
        //        catch (Exception ex)
        //        {
        //            tr.Rollback();
        //            throw ex;
        //        }
        //    }
        //    return i;
        //}
        public IEnumerable<T> QueryModelsWithPropertyName<T>(string str) where T : class
        {
            using (SqlConnection conn = new SqlConnection(_connectstring))
            {
                DataTable dt = new DataTable(typeof(T).Name);
                conn.Open();
                SqlCommand cmd = new SqlCommand(str, conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    var properties = typeof(T).GetProperties();
                    foreach (DataRow item in dt.Rows)
                    {
                        var obj = Activator.CreateInstance<T>();
                        foreach (var property in properties)
                        {
                            if (dt.Columns.Contains(property.Name))
                            {
                                if (item[property.Name] is DBNull)
                                {
                                    continue;
                                }
                                property.SetValue(obj, item[property.Name], null);
                            }
                        }
                        yield return obj;
                    }
                }
                conn.Close();
            }
        }
        public IEnumerable<T> QueryModels<T>(string str) where T : class
        {
            using (SqlConnection conn = new SqlConnection(_connectstring))
            {
                DataTable dt = new DataTable(typeof(T).Name);
                conn.Open();
                SqlCommand cmd = new SqlCommand(str, conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    var properties = TypeDescriptor.GetProperties(typeof(T));
                    foreach (DataRow row in dt.Rows)
                    {
                        var obj = Activator.CreateInstance<T>();
                        foreach (PropertyDescriptor propd in properties)
                        {
                            foreach (Attribute attr in propd.Attributes)
                            {
                                if (attr is ColumnAttribute)
                                {
                                    var col = ((ColumnAttribute)attr).ColumnName;
                                    if (dt.Columns.Contains(col))
                                    {
                                        if (row[col] is DBNull)
                                        {
                                            continue;
                                        }
                                        propd.SetValue(obj, row[col]);
                                    }
                                }
                            }
                        }
                        yield return obj;
                    }
                }
                conn.Close();
            }
        }
        public IEnumerable<T> FindTable<T>(string tableName = null) where T : class
        {
            var type = typeof(T);
            var attributes = TypeDescriptor.GetAttributes(type);
            foreach (Attribute attr in attributes)
            {
                if (attr is TableAttribute)
                {
                    tableName = ((TableAttribute)attr).TableName;
                    break;
                }
            }
            if (tableName == null)
            {
                throw new ArgumentException("Table name can not be null!");
            }
            using (SqlConnection conn = new SqlConnection(_connectstring))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(string.Format("Select * From {0}", tableName), conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    var properties = TypeDescriptor.GetProperties(type);
                    foreach (DataRow row in dt.Rows)
                    {
                        var obj = Activator.CreateInstance<T>();
                        foreach (PropertyDescriptor propd in properties)
                        {
                            foreach (Attribute attr in propd.Attributes)
                            {
                                if (attr is ColumnAttribute)
                                {
                                    var col = ((ColumnAttribute)attr).ColumnName;
                                    if (dt.Columns.Contains(col))
                                    {
                                        if (row[col] is DBNull)
                                        {
                                            continue;
                                        }
                                        propd.SetValue(obj, row[col]);
                                    }
                                }
                            }
                        }
                        yield return obj;
                    }
                }
            }
        }
        public int GetMaxId(string field, string tablename)
        {
            using (SqlConnection conn = new SqlConnection(_connectstring))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(string.Format("select max({0}) from {1}", field, tablename), conn);
                var result = cmd.ExecuteScalar();
                if (result is int)
                {
                    return (int)result;
                }
                else if (result is string)
                {
                    var id = ((string)result).Remove(0, 1);
                    int count = 0;
                    if (int.TryParse(id, out count))
                    {
                        return count;
                    }
                }
                return 0;
            }
        }
        #region 尚未完成
        private int InsertManyModesTVPS<T>(IEnumerable<T> models)
        {
            var dt = ConvertToDataTable(models);
            string tvp;
            var cmdstr = GetSQLString(typeof(T), out tvp);
            using (SqlConnection conn = new SqlConnection(_connectstring))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(cmdstr, conn);
                SqlParameter sqlParameter = cmd.Parameters.AddWithValue(tvp, dt);
                sqlParameter.SqlDbType = SqlDbType.Structured;
                sqlParameter.TypeName = "dbo.BulkUdt";
                return cmd.ExecuteNonQuery();
            }
        }

        private string GetSQLString(Type type, out string tvp)
        {
            StringBuilder sb = new StringBuilder("Insert Into ");
            string tableName = string.Empty;
            var attrs = TypeDescriptor.GetAttributes(type);
            foreach (Attribute attr in attrs)
            {
                if (attr is TableAttribute)
                {
                    tableName = ((TableAttribute)attr).TableName;
                    break;
                }
            }
            if (tableName == string.Empty)
            {
                tableName = type.Name;
            }
            sb.Append(tableName + "(");
            StringBuilder sbvalue = new StringBuilder("Select ");
            var properties = TypeDescriptor.GetProperties(type);
            foreach (PropertyDescriptor pd in properties)
            {
                foreach (Attribute attr in pd.Attributes)
                {
                    if (attr is ColumnAttribute)
                    {
                        var col = ((ColumnAttribute)attr).ColumnName;
                        sb.Append(col + ",");
                        sbvalue.Append("nc." + col + ",");
                    }
                }
            }
            sb.Remove(sb.Length - 1, 1);
            sb.Append(")");
            sbvalue.Remove(sbvalue.Length - 1, 1);
            sbvalue.Append(" From @Tvp as nc");
            sb.Append(sbvalue);
            tvp = "@Tvp";
            return sb.ToString();
        }

        private DataTable ConvertToDataTable<T>(IEnumerable<T> models)
        {
            DataTable dt = new DataTable();
            var properties = TypeDescriptor.GetProperties(typeof(T));
            foreach (PropertyDescriptor pd in properties)
            {
                foreach (Attribute attr in pd.Attributes)
                {
                    if (attr is ColumnAttribute)
                    {
                        var col = ((ColumnAttribute)attr).ColumnName;
                        if (!dt.Columns.Contains(col))
                        {
                            dt.Columns.Add(new DataColumn(col));
                        }
                    }
                }
            }
            foreach (var model in models)
            {
                var row = dt.NewRow();
                foreach (PropertyDescriptor pd in properties)
                {
                    foreach (Attribute attr in pd.Attributes)
                    {
                        if (attr is ColumnAttribute)
                        {
                            var col = ((ColumnAttribute)attr).ColumnName;
                            var obj = pd.GetValue(model);
                            if (obj is bool)
                            {
                                row[col] = (bool)obj ? 1 : 0;
                            }
                            else
                            {
                                row[col] = obj;
                            }
                        }
                    }
                }
                dt.Rows.Add(row);
            }
            return dt;
        }
        #endregion
        public int InsertSequence<T>(T entity)
        {
            var insterStr = InsertModelSQLString(entity);
            using (SqlConnection conn = new SqlConnection(_connectstring))
            {
                conn.Open();

                try
                {
                    SqlCommand cmd = new SqlCommand(insterStr, conn);
                    var count = cmd.ExecuteNonQuery();
                    SqlCommand getid = new SqlCommand("Select @@IDENTITY", conn);
                    var result = getid.ExecuteScalar();
                    return (int)(decimal)result;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
            }
        }
        public int GetCount(string sql)
        {
            using (SqlConnection conn = new SqlConnection(_connectstring))
            {
                conn.Open();
                try
                {
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    var res = cmd.ExecuteScalar();
                    return (int)res;
                }
                catch (Exception)
                {

                    return -1;
                }
                finally
                {
                    conn.Close();
                }
            }
        }
        public int ExecuteSql(string sql)
        {
            using (SqlConnection conn = new SqlConnection(_connectstring))
            {
                conn.Open();
                try
                {
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    var count = cmd.ExecuteNonQuery();
                    return count;
                }
                catch (Exception)
                {
                    return -1;
                }
                finally
                {
                    conn.Close();
                }
            }
        }
    }

}

