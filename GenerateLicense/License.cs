﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Management;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace GenerateLicense
{
    public partial class License : Form
    {
        public License()
        {
            InitializeComponent();
        }


        public static class LicenseHandel
        {
            public static string CreateLicense(string mac)
            {
                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 1; i < 60; i++)
                {
                    mac = MD5Encrypt32(mac);
                    if (i > 50)
                    {
                        stringBuilder.Append(mac);
                    }
                }
                return stringBuilder.ToString();
            }

            private static string MD5Encrypt32(string value)
            {
                string text = "";
                MD5 mD = MD5.Create();
                byte[] array = mD.ComputeHash(Encoding.UTF8.GetBytes(value));
                for (int i = 0; i < array.Length; i++)
                {
                    text += array[i].ToString("X");
                }
                return text;
            }
        }

      
        private void CreateLicense(object sender, EventArgs e)
        {
            string lincense = LicenseHandel.CreateLicense(txtPassword.Text);
            txtLicenseCode.Text = lincense;
        }
    }
}
