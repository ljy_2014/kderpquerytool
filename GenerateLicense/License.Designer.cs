﻿namespace GenerateLicense
{
    partial class License
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.txtLicenseCode = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnGenerateKe = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtLicenseCode
            // 
            this.txtLicenseCode.Location = new System.Drawing.Point(38, 12);
            this.txtLicenseCode.Multiline = true;
            this.txtLicenseCode.Name = "txtLicenseCode";
            this.txtLicenseCode.Size = new System.Drawing.Size(472, 271);
            this.txtLicenseCode.TabIndex = 0;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(111, 311);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(262, 21);
            this.txtPassword.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(49, 315);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "机器码：";
            // 
            // btnGenerateKe
            // 
            this.btnGenerateKe.Location = new System.Drawing.Point(407, 309);
            this.btnGenerateKe.Name = "btnGenerateKe";
            this.btnGenerateKe.Size = new System.Drawing.Size(75, 23);
            this.btnGenerateKe.TabIndex = 3;
            this.btnGenerateKe.Text = "生成密钥";
            this.btnGenerateKe.UseVisualStyleBackColor = true;
            this.btnGenerateKe.Click += new System.EventHandler(this.CreateLicense);
            // 
            // License
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 358);
            this.Controls.Add(this.btnGenerateKe);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtLicenseCode);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "License";
            this.ShowIcon = false;
            this.Text = "授权管理";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtLicenseCode;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnGenerateKe;
    }
}

