﻿namespace MvpCore
{
    public interface IPresenter
    {

    }
    public interface IPresenter<T> : IPresenter where T : class, IView
    {
        T View { get; }
    }
    public abstract class Presenter<T> : IPresenter<T> where T : class, IView
    {
        private readonly T _view;
        protected Presenter(T view)
        {
            _view = view;
        }
        public T View {
            get {
                return _view;
            }
        }
    }
}
