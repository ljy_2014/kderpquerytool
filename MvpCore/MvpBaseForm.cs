﻿using System.Windows.Forms;

namespace MvpCore
{
    public class MvpBaseForm : Form, IView
    {
        private readonly PresentBinder presentBinder = new PresentBinder();
        public MvpBaseForm()
        {
            presentBinder.PerformBinding(this);
        }
    }
}
