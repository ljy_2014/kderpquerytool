﻿namespace MvpCore
{
    public interface IView<T> : IView
    {
        T Model { get; set; }
    }

    public interface IView
    {      
    }  
}
